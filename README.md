# event.diyconspiracy-2409

These are the files for a project for DIY Conspiracy.

There are source files in `sources` created with GIMP and the final results in`examples`. Additionally, a Scribus file is used to lay out a poster and convert RGB to CMYK for printing.

## Usage

### Dependencies

This project depends on *P5js* to work properly, additionally *GIMP* and *Scribus* are used to create the files more easily.

### Process

This is a P5js sketch. Feeding the sketch with a filename, ex. `source.png` looks at that file (must exist). The source file must be black and white only for best results. Processing creates a grid of increasingly bigger squares which it colors according to the brightness of pixels on each square's area. 

1. Create a source file in GIMP or another image creation tool in black and white in the size you want your final result. Put it in the same folder as `sketch.js`

2. In `sketch.js` line `30` write the file name in the `loadImage()` function

3. Run the sketch with P5js installed or in a browser tool. The sketch opens the file and starts creating rows of increasingly bigger `sqPerLine += 1` or smaller `sqPerLine += 1` squares starting from the first `sqPerLine` definition.

4. Right click the canvas and Save As



All images under [Free Art License Copyleft](https://artlibre.org/licence/lal/en/)

All scripts under [GNU GPL v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)
